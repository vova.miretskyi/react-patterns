import { Main } from "./components/pages/Main";

import "./index.css";

function App() {
  return <Main />;
}

export default App;
