import { people, products } from "../../../data/mock-data";

import {
  SplitScreen,
  Right,
  Left,
  RegularList,
  People,
  Products,
  PopupWrap,
} from "../../common";

export const Main = () => {
  return (
    <main
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#000",
        height:'100vh'
      }}
    >
      <PopupWrap></PopupWrap>
      {/* <SplitScreen leftWeight={3} rightWeight={4}>
        <Left title={"Title Left!"} />
        <Right title={"Title Right!"} />
      </SplitScreen>
      <div
        style={{
          width: `100%`,
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
          marginTop: "20px",
        }}
      >
        <h1>People: Small Component</h1>
        <RegularList
          items={people}
          renderItemComponent={({ hairColor, hobbies, ...rest }) => (
            <People.SmallListItem {...rest} />
          )}
        />
      </div>

      <div
        style={{
          width: `100%`,
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
          marginTop: "20px",
        }}
      >
        <h1>People: Small Component</h1>
        <RegularList
          items={people}
          renderItemComponent={(item) => <People.LargeListItem {...item} />}
        />
      </div>
      <div
        style={{
          width: `100%`,
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
          marginTop: "20px",
        }}
      >
        <h1>Products: Large Component</h1>
        <RegularList
          items={products}
          renderItemComponent={(item) => <Products.SmallListItem {...item} />}
        />
        <div
          style={{
            width: `100%`,
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            marginTop: "20px",
          }}
        >
          <h1>Products: Large Component</h1>
          <RegularList
            items={products}
            renderItemComponent={(item) => <Products.LargeListItem {...item} />}
          />
        </div>
      </div> */}
    </main>
  );
};
