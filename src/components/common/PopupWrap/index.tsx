import Button from "../Button";

import css from "./PopupWrap.module.scss";

const PopupWrap = () => {
  return (
    <div className={css.popup}>
      <div className={css.popupBackground}>
        <div className={css.border} />
        <div className={css.contentWrap}>
          <div className={css.header}>
            <div className={css.text}>
              <span>Header</span>
            </div>
          </div>
          <div className={css.childrenWrap}></div>
        </div>
      </div>
    </div>
  );
};

export default PopupWrap;
