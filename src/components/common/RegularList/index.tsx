import React from "react";

interface Props<T> {
  items: T[];
  renderItemComponent: (data: T) => React.ReactElement;
}

const RegularList = <T extends {}>({
  items,
  renderItemComponent,
}: Props<T>) => {
  return <>{items.map((item, i) => renderItemComponent(item))}</>;
};

export default RegularList;
