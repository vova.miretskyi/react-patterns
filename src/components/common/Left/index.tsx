import React, { FC } from "react";

interface Props {
  title: string;
}

const Left: FC<Props> = ({ title }) => {
  return <h1>{title}</h1>;
};

export default Left;
