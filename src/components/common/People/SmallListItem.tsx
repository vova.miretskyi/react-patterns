import { FC } from "react";

interface Props {
  name: string;
  age: number;
}

const SmallListItem: FC<Props> = ({ name, age }) => {
  return (
    <div key={`${name}/${age}`}>
      <p>
        Name: {name}, Age: {age}
      </p>
    </div>
  );
};

export default SmallListItem;
