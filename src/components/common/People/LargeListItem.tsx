import { FC } from "react";

interface Props {
  name: string;
  age: number;
  hairColor: string;
  hobbies: string[];
}

const LargeListItem: FC<Props> = ({ name, age, hairColor, hobbies }) => {
  return (
    <div key={`${name}/${age}`}>
      <h3>{name}</h3>
      <p>Age: {age} years</p>
      <p>Hair Color: {hairColor}</p>
      <ul>
        {hobbies.map((hobby) => (
          <li key={hobby}>{hobby}</li>
        ))}
      </ul>
    </div>
  );
};

export default LargeListItem;
