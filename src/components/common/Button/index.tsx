import React from "react";

import css from "./Button.module.scss";

const Button = () => {
  return (
    <div className={css.btn}>
      <span>Log in</span>
    </div>
  );
};

export default Button;
