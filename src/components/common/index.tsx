import Right from "./Right";
import Left from "./Left";
import SplitScreen from "./SplitScreen";
import RegularList from "./RegularList";
import PopupWrap from "./PopupWrap";
import Button from "./Button";

import * as People from "./People";
import * as Products from "./Products";

export {
  Right,
  Left,
  SplitScreen,
  People,
  Products,
  RegularList,
  PopupWrap,
  Button,
};
