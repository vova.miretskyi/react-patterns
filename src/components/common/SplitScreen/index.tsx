import React, { FC } from "react";

import styled from "styled-components";

const Container = styled.div`
  display: flex;
`;

const Pane = styled.div``;

interface Props {
  children: React.ReactNode[];
  leftWeight?: number;
  rightWeight?: number;
}

const SplitScreen: FC<Props> = ({
  children,
  leftWeight = 1,
  rightWeight = 1,
}) => {
  const [left, right] = children;
  return (
    <Container>
      <Pane style={{ flex: leftWeight, backgroundColor: "green" }}>{left}</Pane>
      <Pane style={{ flex: rightWeight, backgroundColor: "yellow" }}>
        {right}
      </Pane>
    </Container>
  );
};

export default SplitScreen;
