import { FC } from "react";

interface Props {
  name: string;
  price: string;
  description: string;
  rating: number;
}
const LargeListItem: FC<Props> = ({ name, price, description, rating }) => {
  return (
    <div>
      <h3>{name}</h3>
      <p>{price}</p>
      <div>
        <h4>{description}</h4>
      </div>
      <p>Average Rating: {rating}</p>
    </div>
  );
};

export default LargeListItem;
