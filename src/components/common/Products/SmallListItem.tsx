import { FC } from "react";

interface Props {
  name: string;
  price: string;
}

const SmallListItem: FC<Props> = ({ name, price }) => {
  return (
    <div>
      <h3>
        {name} - {price}
      </h3>
    </div>
  );
};

export default SmallListItem;
